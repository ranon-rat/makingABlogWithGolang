module github.com/ranon-rat/myBlog

go 1.15

require (
	github.com/gomarkdown/markdown v0.0.0-20201113031856-722100d81a8e
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.6

)
